var app = require('express')();
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/', 'game.html'));
});
http.listen(process.env.PORT || 9910, function () {
    console.log('server started on ' + 9910)
});
app.use(express.static('public'));
app.get('/killtheserverbecauseiamtom', function (req, res) {
    res.send("Server Killed.");
    process.exit(1);
});
process.setMaxListeners(0);
//VARIABLES

var colorsArr = ["#801A00", "#FF0000", "#FF7519", "#FFFF00", '#66FF33', "#008A00", "#00FFFF", "#0033CC", "660099",
    "#FF80B5", "#FF00FF", "#000000", "#606060", "#B8B8B8", "#FFFFFF"]; // selectable colors (is copy-pasted to client)
//var masterBoard = []; // the games board
var boardSizes = []; // assembles array of valid board sizes at sever start
var directionsVar = [[+1, -1, 0], [+1, 0, -1], [0, +1, -1],
    [-1, +1, 0], [-1, 0, +1], [0, -1, +1]]; // COORDINATE POINTER HEXGRID DIRECTIONS
var playerCount = 0; // CONNECTED PLAYERS
var totalUsers = 0; //TOTAL PLAYERS SINCE START OF SERVER
//var playerArr = []; // holds data for each players
var gameState = 0; // ticks every second and sends to clients, resets at limit
var allMovesArr = []; // all players moves stored here
var colorCounter = 0;
var precision = 4; // for standard deviation in maths section
var lobbyInstances = {};
var gameInstances = {};
var gameVars = {
    plPerGalaxy: 10,
    gameLength: 600000,
    galaxyHeight: 5000,
    galaxyWidth: 5000,
    gravConstant: 0.0000000006673,
    shipMass: 30,
    edgeBounce: 0.015,
    shipR: 15,
    includedPlanets: ['stations', 'bigSuns', 'suns', 'planets', 'pulsar', 'blackHoles'],
    stationLandR: 100,
    stationDist: 500,
    stationDistInc: 80,
    stationWob: 0,
    bigSuns: {
        count: 1,
//        count: 0,
        minR: 200,
        wob: 100,
        reach: 650,
        den: 0.0005,
        color: 'rgb(255,165,0)',
        imgSize: 1000
    },
    suns: {
        count: 28,
//        count: 0,
        minR: 40,
        wob: 10,
        reach: 300,
        den: 0.08,
        color: 'cyan',
        imgSize: 350
    },
    planets: {
        count: 80,
//        count: 0,
        minR: 8,
        wob: 2,
        reach: 150,
        den: 8,
        color: 'grey',
        imgSize: 256
    },
    pulsar: {
        count: 5,
//        count: 0,
        minR: 3,
        wob: 0,
        reach: 200,
        den: -600,
        color: 'white',
        imgSize: 50
    },
    blackHoles: {
        count: 10,
//        count: 0,
        minR: 2,
        wob: 0,
        reach: 100,
        den: 14000,
        color: 'red',
        imgSize: 50
    },
    stations: {
        count: 20,
        minR: 10,
        wob: 0,
        den: 0,
        reach: 200,
        color: 'gold',
        imgSize: 80
    },
    stationNames: ['Home', 'Genesis', 'Salyut', 'Delta', 'Skylab', 'Knight', 'DOS', 'Falcon', 'Mir', 'Tiangong',
        'Operation', 'OPS', 'Kosmos', 'Dragon', 'Spacebus', 'Atlas', 'Soyaz', 'Minotaur', 'Almaz', 'Freedom'],
    ships: {
        eternity: {
            name: 'Eternity',
            blurb: '',
            maxAcc: 0.013,
            mass: 40,
            fuel: 75,
            boost: 2.175,
            brake: 1.009,
            difficulty: 3,
        },
        scorpio: {
            name: 'Scorpio',
            blurb: '',
            maxAcc: 0.016,
            mass: 40,
            fuel: 90,
            boost: 2.5,
            brake: 1.017,
            difficulty: 7
        },
        dauntless: {
            name: 'Dauntless',
            blurb: '',
            maxAcc: 0.010,
            mass: 25,
            fuel: 60,
            boost: 1.75,
            brake: 1.015,
            difficulty: 2
        },
        intervention: {
            name: 'Intervention',
            blurb: '',
            maxAcc: 0.02,
            mass: 37.5,
            fuel: 100,
            boost: 2,
            brake: 1.008,
            difficulty: 8
        },
        buccaneer: {
            name: 'Buccaneer',
            blurb: '',
            maxAcc: 0.014,
            mass: 42.4,
            fuel: 75,
            boost: 2,
            brake: 1.024,
            difficulty: 4
        },
        shootingstar: {
            name: 'ShootingStar',
            blurb: '',
            maxAcc: 0.019,
            mass: 45,
            fuel: 95,
            boost: 2.5,
            brake: 1.005,
            difficulty: 10
        }
    }
};
// SOCKETS

io.sockets.on('connection', function (socket) {
    ++playerCount;
//    firstUser = socket
    socket.galaxyId = 'home';
    socket.join('home');
    socket.isShowing = 0;
    socket.name = 'Cpt. Noname';
    socket.shipName = 'eternity';
    socket.findGame = findGame;
    socket.on('error', function () {

    });
    socket.on('options', function (homeData) {
        switch (homeData.type) {
            case 'join':
                socket.name = homeData.name;
                if (socket.galaxyId === 'home') {
                    var galaxyJoined = socket.findGame('none', 0);
                }
                break;
            case 'single-player':
                socket.name = homeData.name;
                if (socket.galaxyId === 'home') {
                    var galaxyJoined = socket.findGame('none', 1);
                }
                break;
            case 'self-destruct':
                if (gameInstances[socket.galaxyId] !== undefined &&
                        gameInstances[socket.galaxyId]._active[socket.shipId] !== undefined) {
                    gameInstances[socket.galaxyId]._active[socket.shipId]._stats.hasCollided = 1;
                }
                break;
            case 'ship':
                if (gameInstances[socket.galaxyId] !== undefined &&
                        gameInstances[socket.galaxyId]._active[socket.shipId] !== undefined &&
                        gameVars.ships[homeData.shipName] !== undefined) {
                    gameInstances[socket.galaxyId]._active[socket.shipId]._stats.ship = homeData.shipName;
                    socket.shipName = homeData.shipName;
                    ;
                }
                break;
            case 'exit':
                io.to(socket.galaxyId).emit('occasional-updates', {type: 'disconnect', shipId: socket.shipId});

                socket.leave(socket.galaxyId);
                delete gameInstances[socket.galaxyId]._active[socket.shipId];
                socket.join('home');
                socket.galaxyId = 'home';
                break;
            default:
                break;
        }
    });
    socket.on('show', function (journey) { // marks start of a journey
        if (socket.isShowing === 0) {
            if (journey.type === 'free-look') {
                if (gameInstances[socket.galaxyId]._active[socket.shipId]._journey.state === -5) {
                    gameInstances[socket.galaxyId]._active[socket.shipId]._journey.state = 0;
                    gameInstances[socket.galaxyId]._active[socket.shipId]._xSpeed = 0;
                    gameInstances[socket.galaxyId]._active[socket.shipId]._ySpeed = 0;
                } else {
                    gameInstances[socket.galaxyId]._active[socket.shipId]._journey.state = -5;
                }
            } else {
                socket.isShowing = 1;
                io.sockets.connected[socket.id].emit('occasional-updates', {type: 'show-on', journey: {departing: {x: gameInstances[socket.galaxyId]._journeys[journey.departing].x,
                            y: gameInstances[socket.galaxyId]._journeys[journey.departing].y}},
                    arriving: {x: gameInstances[socket.galaxyId]._journeys[journey.arriving].x,
                        y: gameInstances[socket.galaxyId]._journeys[journey.arriving].y}});
                var clShip = gameInstances[socket.galaxyId]._active[socket.shipId];
                var spawnPoint = gameInstances[socket.galaxyId]._openSpaceFinder(gameVars.shipR, 'ship');
                var points = viewScroller({x: clShip._x, y: clShip._y}, {x: spawnPoint.x,
                    y: spawnPoint.y}, 1000);
                clShip._journey.state = -1;
                var viewCounter = 0;
                var viewInterval = setInterval(function () {
                    if (viewCounter === points.length) {
                        clearInterval(viewInterval);
                    } else {
                        clShip._x = points[viewCounter].x;
                        clShip._y = points[viewCounter].y;
                    }
                    ++viewCounter;
                }, 2)

                setTimeout(function () {
                    var points = viewScroller({x: gameInstances[socket.galaxyId]._journeys[journey.departing].x,
                        y: gameInstances[socket.galaxyId]._journeys[journey.departing].y},
                            {x: gameInstances[socket.galaxyId]._journeys[journey.arriving].x,
                                y: gameInstances[socket.galaxyId]._journeys[journey.arriving].y}, 1000);
                    var viewCounter = 0;
                    var viewInterval = setInterval(function () {
                        if (viewCounter === points.length) {
                            clearInterval(viewInterval);
                        } else {
                            clShip._x = points[viewCounter].x
                            clShip._y = points[viewCounter].y
                        }
                        ++viewCounter;
                    }, 15)
                }, 2000)

                setTimeout(function () {
                    var points = viewScroller({x: gameInstances[socket.galaxyId]._journeys[journey.arriving].x,
                        y: gameInstances[socket.galaxyId]._journeys[journey.arriving].y},
                            {x: gameInstances[socket.galaxyId]._journeys[journey.departing].x,
                                y: gameInstances[socket.galaxyId]._journeys[journey.departing].y}, 1000);
                    var viewCounter = 0;
                    var viewInterval = setInterval(function () {
                        if (viewCounter === points.length) {
                            socket.isShowing = 0;
                            clShip._x = gameInstances[socket.galaxyId]._journeys[journey.departing].x;
                            clShip._y = gameInstances[socket.galaxyId]._journeys[journey.departing].y;
                            clShip._journey.state = 0;
                            clearInterval(viewInterval);
                        } else {
                            clShip._x = points[viewCounter].x;
                            clShip._y = points[viewCounter].y;
                        }
                        ++viewCounter;
                    }, 7.5);
                }, 6000);
            }
        }
    });
    console.log('tester');
    socket.on('journey', function (journey) { // marks start of a journey
        var clShip = gameInstances[socket.galaxyId]._active[socket.shipId];
        if (clShip._journey.state === 0 || clShip._journey.state === -5) {
            clShip._stats.fuel = gameVars.galaxyWidth / 1.75;
            io.sockets.connected[socket.id].emit('occasional-updates', {type: 'start', journey: journey,
                ships: gameInstances[socket.galaxyId]._active});
//            socket.journey = journey;
            clShip._journey = {state: -6, launchTime: 0, time: 0, stage: 1};
            socket.journey = {state: -6, launchTime: 0, time: 0, stage: 1};
            clShip._xSpeed = 0.0001;
            clShip._ySpeed = 0.0001;
            var spawnPoint = gameInstances[socket.galaxyId]._openSpaceFinder(gameVars.shipR, 'ship');
            io.sockets.connected[socket.id].emit('occasional-updates', {type: 'scroll', spawnPoint: spawnPoint});
            clShip._x = spawnPoint.x;
            clShip._y = spawnPoint.y;
            setTimeout(function () {
                try {
                    if (io.sockets.connected[socket.id] === undefined) {
                        throw 'player has left';
                    }
                    clShip._journey.state = 4;
                } catch (err) {
                }
            }, 5000);
            setTimeout(function () {
                try {
                    if (io.sockets.connected[socket.id] === undefined) {
                        throw 'player has left';
                    }
                    clShip._journey.state = 3;
                    io.sockets.connected[socket.id].emit('occasional-updates', {type: 'engines-on'});
                    clShip._journey.launchTime = new Date().getTime();
                } catch (err) {
                }
            }, 7500);
            setTimeout(function () {
                try {
                    if (io.sockets.connected[socket.id] === undefined || clShip._journey.state !== 3) {
                        throw 'player has left2';
                    }
                    clShip._journey.state = 2;
                    io.sockets.connected[socket.id].emit('occasional-updates', {type: 'anti-grav'});
                } catch (err) {
                }
            }, 10000);
            setTimeout(function () {
                try {
                    if (io.sockets.connected[socket.id] === undefined || clShip._journey.state !== 2) {
                        throw 'player has left2';
                    }
                    clShip._journey.state = 1;
                    io.sockets.connected[socket.id].emit('occasional-updates', {type: 'anti-ship'});
                } catch (err) {
                }
            }, 12500);
        }
    });
    socket.on('ping', function (msg) {
//        setTimeout(function () {
        io.sockets.connected[socket.id].emit('pong', '');
//
//        }, 100)
    });
    socket.on('chat', function (msg) {
        var chatter = msg.msg.substring(0, 8);
        var chatter2 = msg.msg.substring(0, 10);
        if (chatter === 'adminTom' || socket.admin === 1) { // if admin or not bounce chat back to room
            if (chatter2 === 'adminTomOn') {
                io.to(this.galaxyId).emit('chat', {name: socket.name, msg: 'Admin On'});
                socket.admin = 1;
            }
            if (chatter2 === 'adminTomOff') {
                io.to(this.galaxyId).emit('chat', {name: socket.name, msg: 'Admin Off'});
                io.to(socket.galaxyId).emit('chat', 'Admin Off');
                socket.admin = 0;
            }
            if (chatter !== 'adminTom') {
                var selectedVar = '';
                var value = '';
                for (chatInt = 0; chatInt < msg.msg.length; ++chatInt) {
                    if (msg.msg[chatInt] === '=') {
                        selectedVar = msg.msg.substring(0, chatInt - 1);
                        value = parseInt(msg.msg.substring(chatInt + 2));
                        break;
                    }
                }
                io.to(this.galaxyId).emit('chat', {name: msg.name, msg: msg.msg});
                gameVars[selectedVar] = value;
            }
        } else {
            msg.name = socket.name;
            io.to(socket.galaxyId).emit('chat', msg);
        }
    });
    socket.on('input', function (input) {
//        gameInstances[socket.galaxyId]._active[socket.shipId]._input = input;
    });
    socket.on('mouse-input', function (input) {
        gameInstances[socket.galaxyId]._active[socket.shipId]._input = input;
    });
    socket.on('disconnect', function () {
        if (gameInstances[socket.galaxyId] !== undefined) {
//            io.to(socket.galaxyId).emit('chat', 'has left the game');
            io.to(socket.galaxyId).emit('occasional-updates', {type: 'disconnect', shipId: socket.shipId});
            delete gameInstances[socket.galaxyId]._active[socket.shipId];
            socket.leave(socket.galaxyId);
//            if (Object.keys(gameInstances[socket.galaxyId]._active).length === 0) {
//                
//                delete gameInstances[socket.galaxyId];
//            }
        }
        --playerCount;
    });
//    testUsers();
});

function viewScroller(a, b, time) {
    var viewCoords = [];
    var xDist = a.x + 5 - b.x;
    var yDist = a.y - b.y;
    var xIncrement = -xDist / 200;
    var yIncrement = -yDist / 200;
    var newDistan = 9999999;
    for (k = 1; k < 200; k++) { // sample points along a line until we hit scenery or go over 100px
        var firstCoord = a.x + 5 + xIncrement * k;
        var secondCoord = a.y + yIncrement * k;
        if (newDistan < distance({x: firstCoord, y: secondCoord}, b)) {
            break;
        }
        viewCoords.push({x: firstCoord, y: secondCoord})
        newDistan = distance({x: firstCoord, y: secondCoord}, b);
    }
    return viewCoords;
}

function findGame(galaxyId, sp) { // find empty game, triggers create if no spaces found
    var hasSpaces = [];
    if (this.name === '') {
        this.name = 'Cpt. ' + String(Math.floor((Math.random() * 99999) + 9999));
    }
    if (galaxyId === 'none' && this.galaxyId === 'home') {
        this.leave('home');
        var findGames = Object.keys(gameInstances);
        this.galaxyId = '';
        for (fiGaInt = 0; fiGaInt < findGames.length; ++fiGaInt) {
            if (Object.keys(gameInstances[findGames[fiGaInt]]._active).length <
                    gameInstances[findGames[fiGaInt]]._stats.maxPl &&
                    gameInstances[findGames[fiGaInt]]._stats.maxPl !== 1 && sp !== 1) {
                hasSpaces.push(gameInstances[findGames[fiGaInt]]._id);
            }
        }
        if (hasSpaces.length === 0) {
            this.galaxyId = createGalaxy(sp);
        } else {
            this.galaxyId = hasSpaces[Math.floor((Math.random() * hasSpaces.length) + 0)];
        }
        this.join(this.galaxyId);
    } else {
        this.galaxyId = galaxyId;
    }
    this.shipId = idMaker();
    var xSpawn = 1000;
    var ySpawn = 1000;
    if (gameInstances[this.galaxyId]._stats.startTime + gameVars.gameLength > new Date().getTime()) {
        xSpawn = gameInstances[this.galaxyId]._planets[0].x;
        ySpawn = gameInstances[this.galaxyId]._planets[0].y;
    }
    gameInstances[this.galaxyId]._active[this.shipId] = new Ship({time: 0, stage: 0},
            xSpawn, ySpawn, 0.001, 0.001,
            this.id, this.shipId, {}, {x: 0, y: 100, angle: 90, click: {l: 0, r: 0}},
            {state: 0, launchTime: 0, time: 0, stage: 1},
            {name: this.name, ship: this.shipName, fuel: 0, onTheGas: 0, angle: 90, speed: 0,
                hasCollided: 0, grav: {x: 1, y: 1}, sp: sp});

    io.sockets.connected[this.id].emit('initial-game', {gameVars: gameVars,
        galaxy: gameInstances[this.galaxyId], shipId: this.shipId,
        journeys: gameInstances[this.galaxyId]._journeys});
    io.to(this.galaxyId).emit('chat', {name: this.name, msg: 'has joined the game'});
    return this.galaxyId;
}

function createGalaxy(sp) {
    if (sp === 0) {
        maxPl = gameVars.plPerGalaxy;
    } else {
        maxPl = 1;
    }
    var galaxyId = idMaker();
    gameInstances[galaxyId] = new Galaxy({width: gameVars.galaxyWidth, height: gameVars.galaxyHeight,
        startTime: new Date().getTime(), spawn: {x: Math.floor((Math.random() * gameVars.galaxyWidth - 50) + 50),
            y: Math.floor((Math.random() * gameVars.galaxyHeight - 50) + 50)}, maxPl: maxPl},
            createPlanets, [], [], {}, galaxyId, 'interval', 'colors', motion, {}, [],
            addPickups, openSpaceFinder);
    gameInstances[galaxyId]._createPlanets();
    gameInstances[galaxyId]._journeys = journeyInit(galaxyId);
    gameOverGameStart(galaxyId);
    return galaxyId;
}



function gameOverGameStart(galaxyId) {
    var lastFrame = process.hrtime();
    var physicsEngine = setInterval(function () { // causes one step in motion and checks collisions
        gameInstances[galaxyId]._motion(lastFrame);
        lastFrame = process.hrtime();
    }, 10);

    var updateEngine = setInterval(function () { // sends position data to clients
        io.to(galaxyId).emit('client-updates', {ships: gameInstances[galaxyId]._active,
            pickups: gameInstances[galaxyId]._pickups,
            time: new Date().getTime() - gameInstances[galaxyId]._stats.startTime});
    }, 40);

    var pickupEngine = setInterval(function () {
//        gameInstances[galaxyId]._addPickups();
    }, 5000);

    setTimeout(function () {
        io.to(galaxyId).emit('chat', {name: 'Game time remaining', msg: '60 seconds!!'});
    }, gameVars.gameLength - 60000);
    setTimeout(function () {
        io.to(galaxyId).emit('chat', {name: 'Game time remaining', msg: '30 seconds!!'});
    }, gameVars.gameLength - 30000);

    setTimeout(function () {
        io.to(galaxyId).emit('occasional-updates', {type: 'game-over', ships: gameInstances[galaxyId]._active});
        io.to(galaxyId).emit('chat', {name: 'Game Over', msg: ''});
        clearInterval(physicsEngine);
        clearInterval(updateEngine);
        clearInterval(pickupEngine);
        var finishingPlayers = Object.keys(gameInstances[galaxyId]._active);
        for (finPlInt = 0; finPlInt < finishingPlayers.length; ++finPlInt) {
            gameInstances[galaxyId]._active[finishingPlayers[finPlInt]]._xSpeed = 0.0001;
            gameInstances[galaxyId]._active[finishingPlayers[finPlInt]]._ySpeed = 0.0001;
            gameInstances[galaxyId]._active[finishingPlayers[finPlInt]]._journey.state = 0;
        }
        io.to(galaxyId).emit('client-updates', {ships: gameInstances[galaxyId]._active, pickups: gameInstances[galaxyId]._pickups});
        gameInstances[galaxyId]._planets = [];
        gameInstances[galaxyId]._pickups = [];
        setTimeout(function () {
            var remainingPlStore = gameInstances[galaxyId]._active;
            var remainingPl = Object.keys(remainingPlStore);
            if (remainingPl.length > 0) {
                var remainingPlStore = gameInstances[galaxyId]._active;
                gameInstances[galaxyId]._active = {};
                gameInstances[galaxyId]._createPlanets();
                gameInstances[galaxyId]._journeys = journeyInit(galaxyId);
                gameInstances[galaxyId]._stats.startTime = new Date().getTime();
                gameOverGameStart(galaxyId);
                var remainingPl = Object.keys(remainingPlStore);
                for (remPlInt = 0; remPlInt < remainingPl.length; ++remPlInt) {
                    if (io.sockets.connected[remainingPlStore[remainingPl[remPlInt]]._owner] !== undefined) {
                        io.sockets.connected[remainingPlStore[remainingPl[remPlInt]]._owner].
                                findGame(galaxyId, gameInstances[galaxyId]._stats.maxPl);
                    }
                }
            } else {
                delete gameInstances[galaxyId];
            }
        }, 20000) // time spent looking at scores
    }, gameVars.gameLength); // time until gameover declared
}

function addPickups() {
    var plaCount = Object.keys(this._active).length;
    if (this._pickups.length / plaCount < 10) {
        for (pickInt = 0; pickInt < plaCount; pickInt++) {
            var position1 = this._openSpaceFinder(10);
            var position2 = this._openSpaceFinder(10);
            var position3 = this._openSpaceFinder(10);
            this._pickups.push({type: 'fuel', x: position1.x, y: position1.y},
                    {type: 'cargo', x: position2.x, y: position2.y}, {type: 'cargo', x: position3.x, y: position3.y});
        }
    }
}

function journeyInit(galaxyId) {
    var journeys = {};
    var plan = gameInstances[galaxyId]._planets;
    for (joInt = 0; joInt < gameVars.stations.count; ++joInt) {
        journeys[gameVars.stationNames[joInt]] = {x: plan[joInt].x
            , y: plan[joInt].y}
    }
    return journeys;
}

function gravityMapper(galaxyId) {
    gameInstances[galaxyId]._gravArr = new Array(gameVars.galaxyHeight);
    for (i = 0; i < gameInstances[galaxyId]._gravArr.length; i++) {
        gameInstances[galaxyId]._gravArr[i] = new Array(gameVars.galaxyWidth);
        for (j = 0; j < gameInstances[galaxyId]._gravArr[i].length; j++) {
            gameInstances[galaxyId]._gravArr[i][j] = {};
        }
    }
}

function createPlanets() {
    for (crPlInt = 0; crPlInt < gameVars.includedPlanets.length; ++crPlInt) {
        for (crPlInt2 = 0; crPlInt2 < parseInt(gameVars[gameVars.includedPlanets[crPlInt]].count); ++crPlInt2) {
            var st = gameVars[gameVars.includedPlanets[crPlInt]];
            var rad = Math.floor((Math.random() * st.wob + st.minR) + st.minR);
            var openSpace = this._openSpaceFinder(rad, gameVars.includedPlanets[crPlInt]);
            this._planets.push({radius: rad, type: gameVars.includedPlanets[crPlInt],
                mass: 4 / 3 * Math.PI * Math.pow(rad, 3) * st.den, color: st.color,
                x: openSpace.x, y: openSpace.y
            })
            if (crPlInt === gameVars.includedPlanets.length - 1) {
                this._planets[this._planets.length - 1].name = gameVars.stationNames[crPlInt2];
            }
        }
    }
    this._planets[0].reach = 250;
}

function openSpaceFinder(size, type) { // finds a position for a new body that is not overlapping a planet/station
    var xCoord = 0;
    var yCoord = 0;
    var validSpace = false;
    while (validSpace === false) {
        xCoord = Math.floor((Math.random() * gameVars.galaxyWidth) + 0);
        yCoord = Math.floor((Math.random() * gameVars.galaxyHeight) + 0);

        if (type === 'stations') {
            if (this._planets.length > 0) {
                var randAngle = Math.PI / 180 * Math.random() * 360;
                xCoord = Math.round(this._planets[this._planets.length - 1].x +
                        (Math.floor((Math.random() * gameVars.stationWob) + 0) + (gameVars.stationDist +
                                gameVars.stationDistInc * this._planets.length)) * Math.cos(randAngle));
                yCoord = Math.round(this._planets[this._planets.length - 1].y +
                        (Math.floor((Math.random() * gameVars.stationWob) + 0) + (gameVars.stationDist +
                                gameVars.stationDistInc * this._planets.length)) * Math.sin(randAngle));
            } else {
                var randAngle = Math.PI / 180 * Math.random() * 360;
                xCoord = Math.floor((Math.random() * gameVars.galaxyWidth - 50) + 50);
                yCoord = Math.floor((Math.random() * gameVars.galaxyHeight - 50) + 50);
            }
        }
        if (type === 'ship') {
            var randAngle = Math.PI / 180 * Math.random() * 360;
            xCoord = Math.round(this._planets[0].x +
                    (Math.floor((Math.random() * 250) + 0)) * Math.cos(randAngle));
            yCoord = Math.round(this._planets[0].y +
                    (Math.floor((Math.random() * 250) + 0)) * Math.sin(randAngle));

            for (crPlInt4 = 0; crPlInt4 < this._active.length; ++crPlInt4) {
                if (distance({x: xCoord, y: yCoord}, {x: this._active[crPlInt4]._x, y: this._active[crPlInt4]._y})
                        < this._active[crPlInt4].radius + size + 25) {
                    validSpace = false;
                }
            }
        }
        validSpace = true;
        for (crPlInt3 = 0; crPlInt3 < this._planets.length; ++crPlInt3) {
            if (crPlInt === gameVars.includedPlanets.length - 1) {
                extraR = gameVars['stations'].minR;
            }
            if (distance({x: xCoord, y: yCoord}, {x: this._planets[crPlInt3].x, y: this._planets[crPlInt3].y})
                    < this._planets[crPlInt3].radius + size) {
                validSpace = false;
            }
            if (type === 'stations' && distance({x: xCoord, y: yCoord}, {x: this._planets[crPlInt3].x,
                y: this._planets[crPlInt3].y})
                    < (this._planets[crPlInt3].radius + gameVars.stations.reach + size) * 2) {
                validSpace = false;
            }
            if (type === 'stations' && (xCoord < 200 || xCoord > gameVars.galaxyWidth - 200 ||
                    yCoord < 200 || yCoord > gameVars.galaxyHeight - 200)) {
                validSpace = false;
            }
            if (type !== 'ship' && type !== 'stations' && distance({x: xCoord, y: yCoord}, {x: this._planets[0].x,
                y: this._planets[0].y}) < size + gameVars[type].reach + 250) {
                validSpace = false;
            }
            if (type !== 'ship' && type !== 'stations' && this._planets[crPlInt3].type !== 'stations' &&
                    distance({x: xCoord, y: yCoord}, {x: this._planets[crPlInt3].x, y: this._planets[crPlInt3].y}) <
                    gameVars[this._planets[crPlInt3].type].reach + this._planets[crPlInt3].radius +
                    gameVars[type].reach / 2) {
                validSpace = false;
            }
        }
        if (xCoord < 0 || xCoord > gameVars.galaxyWidth || yCoord < 0 || yCoord > gameVars.galaxyHeight) {
            validSpace = false;
        }
    }
    return{x: xCoord, y: yCoord};
}

function heronsFormula(sideA, sideB, sideC) {
    var sides = (sideA + sideB + sideC) / 2;
    var triArea = Math.sqrt(((sideA + (sideB + sideC)) * (sideC - (sideA - sideB)) * (sideC + (sideA - sideB)) * (sideA + (sideB - sideC)))) / 4;
    return triArea;
}

function motion(lastFrame) {
    var delta = process.hrtime(lastFrame);
    var moShips = Object.keys(this._active);
    for (moInt = 0; moInt < moShips.length; ++moInt) {
        if (this._active[moShips[moInt]]._stats.sp !== 1 &&
                (this._active[moShips[moInt]]._journey.state >= 0 && this._active[moShips[moInt]]._journey.state !== 4) ||
                this._active[moShips[moInt]]._journey.state === -5) {
            var onTheGas = 0;
            var xDistortion = 0;
            var yDistortion = 0;
            var inputAcc = {x: 0, y: 0};
            var maxAcc = 0;
            var fuelConsumed = 0;
            if (this._active[moShips[moInt]]._x < 0) {
                maxAcc = 0.0015;
                xDistortion += gameVars.edgeBounce;
                onTheGas = 2;
            }
            if (this._active[moShips[moInt]]._x > gameVars.galaxyWidth) {
                maxAcc = 0.0015;
                xDistortion -= gameVars.edgeBounce;
                onTheGas = 2;
            }
            if (this._active[moShips[moInt]]._y < 0) {
                maxAcc = 0.0015;
                yDistortion += gameVars.edgeBounce;
                onTheGas = 2;
            }
            if (this._active[moShips[moInt]]._y > gameVars.galaxyHeight) {
                maxAcc = 0.0015;
                yDistortion -= gameVars.edgeBounce;
                onTheGas = 2;
            }
            if (this._active[moShips[moInt]]._stats.fuel < 3 && this._active[moShips[moInt]]._stats.fuel !== 0) {
                this._active[moShips[moInt]]._stats.fuel = 0;
                io.sockets.connected[this._active[moShips[moInt]]._owner].emit('occasional-updates', {type: 'no-fuel'});
            }
            if ((onTheGas !== 2 && this._active[moShips[moInt]]._stats.fuel > 0 &&
                    this._active[moShips[moInt]]._journey.state > 0) ||
                    this._active[moShips[moInt]]._journey.state === -5) {
                if (this._active[moShips[moInt]]._input.click.r === 1) {
                    maxAcc = gameVars.ships[this._active[moShips[moInt]]._stats.ship].maxAcc *
                            gameVars.ships[this._active[moShips[moInt]]._stats.ship].boost;
                    fuelConsumed += 0.5;
                } else if (this._active[moShips[moInt]]._input.click.l === 1) {
//                    fuelConsumed += gameVars.ships[this._active[moShips[moInt]]._stats.ship].fuel;
                    maxAcc = gameVars.ships[this._active[moShips[moInt]]._stats.ship].maxAcc;
                }
                var aveAngle = this._active[moShips[moInt]]._input.angle;
                inputAcc = {x: maxAcc * Math.cos(Math.PI / 180 * aveAngle),
                    y: -maxAcc * Math.sin(Math.PI / 180 * aveAngle)};
                fuelConsumed += gameVars.ships[this._active[moShips[moInt]]._stats.ship].fuel * maxAcc;
//                maxAcc = Math.min(maxAcc * this._active[moShips[moInt]]._input.dist / 200, maxAcc)
//                fuelConsumed += maxAcc;
//                inputAcc = {x: maxAcc * Math.cos(Math.PI / 180 * aveAngle),
//                    y: -maxAcc * Math.sin(Math.PI / 180 * aveAngle)};
            }
            this._active[moShips[moInt]]._stats.angle = aveAngle;
            var dist = 0;
            if (this._active[moShips[moInt]]._journey.state < 3 && this._active[moShips[moInt]]._journey.state >= 0) {
                for (ii = 0; ii < this._planets.length; ++ii) {// gravitation calcs
                    var dist = distance({x: this._planets[ii].x, y: this._planets[ii].y},
                            {x: this._active[moShips[moInt]]._x, y: this._active[moShips[moInt]]._y});
                    if (dist < parseInt(gameVars[this._planets[ii].type].reach + this._planets[ii].radius) &&
                            dist > 0) {
                        var gravity = gameVars.gravConstant * gameVars.ships[this._active[moShips[moInt]]._stats.ship].mass *
                                this._planets[ii].mass / dist * dist;

                        var angle = findAngle(this._planets[ii], {x: this._active[moShips[moInt]]._x,
                            y: this._active[moShips[moInt]]._y}, dist);
                        var xPerc = gravity * Math.cos(Math.PI / 180 * angle);
                        var yPerc = gravity * Math.sin(Math.PI / 180 * angle);
                        xDistortion -= xPerc;
                        yDistortion -= yPerc;
                    }
                }
                this._active[moShips[moInt]]._stats.grav.x = xDistortion;
                this._active[moShips[moInt]]._stats.grav.y = yDistortion;
            }
            if (onTheGas === 2) {
//                io.sockets.connected[this._active[moShips[moInt]]._owner].emit('occasional-updates', {type: 'out-of-bounds'});
                fuelConsumed += 2;
            }
            this._active[moShips[moInt]]._stats.fuel -= fuelConsumed;
            this._active[moShips[moInt]]._stats.onTheGas = onTheGas;
            this._active[moShips[moInt]]._stats.grav.x = Math.round(this._active[moShips[moInt]]._stats.grav.x * 100000) / 1000000;
            this._active[moShips[moInt]]._stats.grav.y = Math.round(this._active[moShips[moInt]]._stats.grav.y * 100000) / 1000000;
//            if (this._active[moShips[moInt]]._stats.onTheGas !== 2) {

            this._active[moShips[moInt]]._xSpeed += (xDistortion + inputAcc.x) * delta[1] * 1e-7;
            this._active[moShips[moInt]]._ySpeed += (yDistortion + inputAcc.y) * delta[1] * 1e-7;//            }
            this._active[moShips[moInt]]._x += this._active[moShips[moInt]]._xSpeed * delta[1] * 1e-7;
            this._active[moShips[moInt]]._y += this._active[moShips[moInt]]._ySpeed * delta[1] * 1e-7;
//            this._active[moShips[moInt]]._xSpeed = 1.5
            this._active[moShips[moInt]]._xSpeed = Math.round(this._active[moShips[moInt]]._xSpeed * 1000) / 1000;
            this._active[moShips[                      moInt]]._ySpeed = Math.round(this._active[moShips[moInt]]._ySpeed * 1000) / 1000;
            this._active[moShips[moInt]]._x = Math.round(this._active[moShips[moInt]]._x * 100) / 100;
            this._active[moShips[moInt]]._y = Math.round(this._active[moShips[moInt]]._y * 100) / 100;
            if (this._active[moShips[moInt]]._stats.fuel < 0) {
                this._active[moShips[moInt]]._stats.fuel = 0;
            }

            for (ii = 0; ii < this._planets.length; ++ii) {
                var dist = distance({x: this._planets[ii].x, y: this._planets[ii].y},
                        {x: this._active[moShips[moInt]]._x, y: this._active[moShips[moInt]]._y});
                if (this._planets[ii].type === 'stations' && dist <= this._planets[ii].radius +
                        gameVars[this._planets[ii].type].reach + gameVars.shipR &&
                        this._active[moShips[moInt]]._journey.state > 0
                        && ii === this._active[moShips[moInt]]._journey.stage) {
                    //landing at a station
                    var newScore = this._active[moShips[moInt]]._journey.stage * 1000 -
                            this._active[moShips[moInt]]._journey.time;
                    var oldScore = this._active[moShips[moInt]]._score.stage * 1000 -
                            this._active[moShips[moInt]]._score.time;
                    this._active[moShips[moInt]]._journey.time = Math.round((new Date().getTime() -
                            this._active[moShips[moInt]]._journey.launchTime) / 10) / 100;
                    if (newScore > oldScore || this._active[moShips[moInt]]._score.time === 0) {
                        this._active[moShips[moInt]]._score = {stage: this._active[moShips[moInt]]._journey.stage,
                            time: this._active[moShips[moInt]]._journey.time};
                    }
                    this._active[moShips[moInt]]._stats.fuel += 120;
                    this._active[moShips[moInt]]._journey.stage += 1;
                    io.sockets.connected[this._active[moShips[moInt]]._owner].emit('occasional-updates',
                            {type: 'success-text', x: this._active[moShips[moInt]]._x, y: this._active[moShips[moInt]]._y,
                                journey: this._active[moShips[moInt]]._journey, size: 50, duration: 4000,
                                value: '+' + gameVars.stationNames[this._active[moShips[moInt]]._journey.stage - 1]});

                    if (this._active[moShips[moInt]]._journey.stage === gameVars.stations.count) {
                        io.sockets.connected[this._active[moShips[moInt]]._owner].emit('occasional-updates',
                                {type: 'fail', name: this._active[moShips[moInt]]._stats.name,
                                    journey: this._active[moShips[moInt]]._journey, score: this._active[moShips[moInt]]._score,
                                    playerCount: playerCount});
                        this._active[moShips[moInt]]._journey.stage = 1;
                        this._active[moShips[moInt]]._journey.state = 0;
                        this._active[moShips[moInt]]._xSpeed = 0;
                        this._active[moShips[moInt]]._ySpeed = 0;
                        this._active[moShips[moInt]]._stats.hasCollided = 0;
                        this._active[moShips[moInt]]._stats.onTheGas = 0;
                        this._active[moShips[moInt]]._stats.fuel = 0;
                        io.sockets.connected[this._active[moShips[moInt]]._owner].emit(
                                'occasional-updates', {type: 'completion', journey: this._active[moShips[moInt]]._journey});
                    }
                }
                if (dist <= this._planets[ii].radius + gameVars.shipR && this._active[moShips[moInt]]._journey.state > 0 &&
                        this._planets[ii].type !== 'stations') {
                    this._active[moShips[moInt]]._stats.hasCollided = 1;
                } // above section planet-ship overlap detected
            }
            if (this._active[moShips[moInt]]._journey.state > 0) {
                for (moInt2 = 0; moInt2 < moShips.length; ++moInt2) { // player-player collision detection
                    if (moInt !== moInt2) {
                        var distShips = distance({x: this._active[moShips[moInt]]._x, y: this._active[moShips[moInt]]._y},
                                {x: this._active[moShips[moInt2]]._x, y: this._active[moShips[moInt2]]._y});
                        if (distShips < gameVars.shipR * 2 && this._active[moShips[moInt]]._journey.state === 1 &&
                                this._active[moShips[moInt2]]._journey.state === 1) {
                            this._active[moShips[moInt]]._stats.hasCollided = 1;
                            this._active[moShips[moInt2]]._stats.hasCollided = 1;
                        }
                    }
                }
                if (this._active[moShips[moInt]]._stats.hasCollided === 1) { // action if collision detected
                    io.sockets.connected[this._active[moShips[moInt]]._owner].emit('occasional-updates',
                            {type: 'fail', name: this._active[moShips[moInt]]._stats.name,
                                journey: this._active[moShips[moInt]]._journey, score: this._active[moShips[moInt]]._score,
                                playerCount: playerCount});
                    io.to(this._id).emit('occasional-updates',
                            {type: 'explosion', img: 'explosion', x: this._active[moShips[moInt]]._x, y: this._active[moShips[moInt]]._y,
                                xSpeed: this._active[moShips[moInt]]._xSpeed, ySpeed: this._active[moShips[moInt]]._ySpeed,
                                time: 0, size: 50, duration: 750});
                    this._active[moShips[moInt]]._journey.state = 0;
                    this._active[moShips[moInt]]._xSpeed = 0;
                    this._active[moShips[moInt]]._ySpeed = 0;
                    this._active[moShips[moInt]]._stats.hasCollided = 0;
                    this._active[moShips[moInt]]._stats.fuel = 0;
                    this._active[moShips[moInt]]._stats.onTheGas = 0;
                }
            }
        }
    }
//    io.to(this._id).emit('galaxy-layout', gameInstances[this._id]);
}

function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}
function pythagasaurusRex(a, b) {    // 
    return Math.sqrt((Math.abs(a * a) + Math.abs(b * b)))
}


function distance(a, b) {
    return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
}

function distance2(a, b) {
    return Math.sqrt(Math.pow(Math.abs(b.x) - Math.abs(a.x), 2) + Math.pow(Math.abs(b.y) - Math.abs(a.y), 2));
}

function findAngle(a, b, c) { // from two points and a distance, C
    var angle = 180 / 3.14 * Math.acos((a.y - b.y) / c);
    if (a.x > b.x) {
        angle *= -1;
    }
    angle += 270
    if (angle >= 360) {
        angle -= 360;
    }

    return angle;
}


//CREATE ELEMENTS

function Galaxy(stats, createPlanets, gravArr, planets, active, id, interval, colors, motion, journeys, pickups,
        addPickups, openSpaceFinder) {
    this._stats = stats;
    this._createPlanets = createPlanets;
    this._gravArr = gravArr;
    this._planets = planets;
    this._active = active;
    this._id = id;
    this._interval = interval;
    this._colors = colors;
    this._motion = motion;
    this._journeys = journeys;
    this._pickups = pickups;
    this._addPickups = addPickups;
    this._openSpaceFinder = openSpaceFinder;
}

Galaxy.prototype.set = function fn1() {
    return this._stats;
    return this._createPlanets;
    return this._gravArr;
    return this._planets;
    return this._active;
    return this._id;
    return this._interval;
    return this._colors;
    return this._motion;
    return this._journeys;
    return this._pickups;
    return this._addPickups;
    return this._openSpaceFinder;
};
function NewGalaxy(stats, createPlanets, gravArr, owner, active, id, interval, colors, motion, journeys, pickups,
        addPickups, openSpaceFinder) {
    Galaxy.call(this, stats, createPlanets, gravArr, owner, active, id, interval, colors, motion, journeys, pickups,
            addPickups, openSpaceFinder);
}

NewGalaxy.prototype =
        Object.create(Galaxy.prototype);
NewGalaxy.prototype.constructor = NewGalaxy;
NewGalaxy.prototype.set = function fn2() {
    return Galaxy.prototype.set.call(this);
};
function Ship(score, x, y, xSpeed, ySpeed, owner, id, color, input, journey, stats) { // character constructor
    this._score = score;
    this._x = x;
    this._y = y;
    this._xSpeed = xSpeed;
    this._ySpeed = ySpeed;
    this._owner = owner;
    this._id = id;
    this._color = color;
    this._input = input;
    this._journey = journey;
    this._stats = stats;
}

Ship.prototype.set = function fn1() {
    return this._score;
    return this._x;
    return this._y;
    return this._xSpeed;
    return this._ySpeed;
    return this._owner;
    return this._id;
    return this._color;
    return this._input;
    return this._journey;
    return this._stats;
};
function NewShip(score, x, y, xSpeed, ySpeed, owner, id, color, input, journey, stats) {
    Stuff.call(this, score, x, y, xSpeed, ySpeed, owner, id, color, input, journey, stats);
}

NewShip.prototype =
        Object.create(Ship.prototype);
NewShip.prototype.constructor = NewShip;
NewShip.prototype.set = function fn2() {
    return Ship.prototype.set.call(this);
};
var idMaker = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
}
;
function Stuff(ph, row, col, owner, active, id, size, colors, movePiece) {
    this._ph = ph;
    this._row = row;
    this._col = col;
    this._owner = owner;
    this._active = active;
    this._id = id;
    this._size = size;
    this._colors = colors;
    this._movePiece = movePiece;
}

Stuff.prototype.set = function fn1() {
    return this._ph;
    return this._row;
    return this._col;
    return this._owner;
    return this._active;
    return this._id;
    return this._size;
    return this._colors;
    return this._movePiece;
};
function NewStuff(ph, row, col, owner, active, id, size, colors, movePiece) {
    Stuff.call(this, ph, row, col, owner, active, id, size, colors, movePiece);
}

NewStuff.prototype =
        Object.create(Stuff.prototype);
NewStuff.prototype.constructor = NewStuff;
NewStuff.prototype.set = function fn2() {
    return Stuff.prototype.set.call(this);
};
var idMaker = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
}


//var falsePlayer = {};
//var firstUser = {};
//
//function testUsers(){
//setInterval(function(){
//    falsePlayer = firstUser
//    falsePlayer.galaxyId = 'home';
////    falsePlayer.join('home');
//    falsePlayer.isShowing = 0;
//    falsePlayer.name = 'Cpt. Noname';
//    falsePlayer.shipName = 'eternity';
//    falsePlayer.findGame = findGame;
//    falsePlayer.findGame('none')
//    
//},10)
//}

// USEFUL DEBUG STUFF


// KEYBOARD CONTROLS 



//                if (this._active[moShips[moInt]]._input['87'] === true) {
//                    this._active[moShips[moInt]]._ySpeed -= 0.015;
//                    inputAcc = {x: 0, y: -0.015};
//                }
//                //up right
//                if (this._active[moShips[moInt]]._input['87'] === true && this._active[moShips[moInt]]._input['68'] === true) {
//                    this._active[moShips[moInt]]._ySpeed -= 0.015;
//                    inputAcc = {x: +0.0106, y: -0.0106};
//                }
//                //up left
//                if (this._active[moShips[moInt]]._input['87'] === true && this._active[moShips[moInt]]._input['65'] === true) {
//                    this._active[moShips[moInt]]._ySpeed -= 0.015;
//                    inputAcc = {x: -0.0106, y: -0.0106};
//                }
//                //down
//                if (this._active[moShips[moInt]]._input['83'] === true) {
//                    this._active[moShips[moInt]]._ySpeed += 0.015;
//                    onTheGas = 1;
//                    inputAcc = {x: 0, y: +0.015};
//                }
//                //down right
//                if (this._active[moShips[moInt]]._input['83'] === true && this._active[moShips[moInt]]._input['68'] === true) {
//                    this._active[moShips[moInt]]._ySpeed += 0.015;
//                    inputAcc = {x: +0.0106, y: -0.0106};
//                }
//                //down left
//                if (this._active[moShips[moInt]]._input['83'] === true && this._active[moShips[moInt]]._input['65'] === true) {
//                    this._active[moShips[moInt]]._ySpeed += 0.015;
//                    inputAcc = {x: -0.0106, y: -0.0106};
//                }
//                //right
//                if (this._active[moShips[moInt]]._input['68'] === true) {
//                    this._active[moShips[moInt]]._xSpeed += 0.015;
//                    onTheGas = 1;
//                    inputAcc = {x: +0.015, y: 0};
//                }
//                //left
//                if (this._active[moShips[moInt]]._input['65'] === true) {
//                    this._active[moShips[moInt]]._xSpeed -= 0.015;
//                    onTheGas = 1;
//                    inputAcc = {x: -0.015, y: 0};
//                }